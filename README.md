# Bitrix24 for Franz

This is the non-official Franz recipe for Bitrix24

![Screenshot](https://s3.eu-central-1.amazonaws.com/b24.help/assets/b24franz.png)

## How to install a recipe

1. To install a integration, download the integration folder e.g `bitrix24`.
2. Open the Franz Plugins folder on your machine (note that this dev directory may not exist yet, and you must create it):
	* Mac: ~/Library/Application Support/Franz/recipes/dev/
	* Windows: %appdata%/Franz/recipes/dev/
	* Linux: ~/.config/Franz/recipes/dev
3. Copy the `bitrix24` folder into the plugins directory
3. Reload Franz