const path = require('path');

module.exports = (Franz, options) => {
	function getMessages() {
		var count = 0;
		var counters = ['bx-desktop-tab-im','bx-desktop-tab-notify'];
		counters.forEach(function(id){
			var counter = document.getElementById(id);
			var badge = counter.getElementsByClassName('bx-desktop-tab-counter-digit');
			for (i = 0; i < badge.length; i++) {
				count += parseInt(badge[i].innerHTML);
			}
		});
		Franz.setBadge(count);
	};
	Franz.loop(getMessages);
	Franz.injectCSS(path.join(__dirname, 'style.css'));
};
